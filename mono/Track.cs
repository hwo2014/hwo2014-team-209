﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

public class Track
{
  public string id = null;
  public string name = null;
  public List<TrackPiece> pieces = null;
  public List<TrackLane> lanes = null;

  public List<TrackSegment> segments = null;

  public Track(string id, string name, List<TrackPiece> pieces, List<TrackLane> lanes)
  {
    this.id = id;
    this.name = name;
    this.pieces = pieces;
    this.lanes = lanes;

    this.CreateSegments();
  }

  public override string ToString()
  {
    string track = "";
    int pieceIndex = 0;
    foreach (TrackPiece piece in this.pieces)
    {
      track += pieceIndex.ToString() + ";";

      if (piece.IsStraight()) track += "straight;";
      else                    track += "curve;";

      track += piece.length.ToString() + ";";
      track += piece.radius.ToString() + ";";
      track += piece.angle.ToString() + ";";
      
      if (piece.switchPiece) track += "switch\n";
      else                   track += "---\n";

      pieceIndex++;
    }

    track += "Segments\n";
    foreach (TrackSegment segment in this.segments)
    {
      track += "Segment;";
      
      if (segment.IsStraight()) track += "straight;";
      else                      track += "curve;";

      track += segment.totalLength.ToString() + ";";
      track += segment.minimumRadius.ToString() + ";";
      track += segment.totalAngle.ToString() + ";";

      foreach (int segmentPiece in segment.pieceIndices)
      {
        track += segmentPiece.ToString() + "#";
      }

      track = track.Substring(0, track.Length - 1);
      track += "\n";
    }

    return track;
  }

  public TrackPiece NextPiece(int currentPiece)
  {
    return this.pieces[this.NextPieceIndex(currentPiece)];
  }

  public int NextPieceIndex(int currentPiece)
  {
    currentPiece++;
    if (currentPiece < 0) currentPiece = 0;
    if (currentPiece > this.pieces.Count - 1) currentPiece = 0;

    return currentPiece;
  }

  public int PreviousPieceIndex(int currentPiece)
  {
    currentPiece--;
    if (currentPiece < 0) currentPiece = this.pieces.Count - 1;
    if (currentPiece > this.pieces.Count - 1) currentPiece = this.pieces.Count - 1;

    return currentPiece;
  }

  public void DriveForward(ref int pieceIndex, ref double pieceDistance, double forward)
  {
    if (pieceIndex >= 0 && pieceIndex < this.pieces.Count)
    {
      pieceDistance += forward;
      while (pieceDistance >= this.ShortestLaneLength(pieceIndex))
      {
        pieceDistance -= this.ShortestLaneLength(pieceIndex);
        pieceIndex = this.NextPieceIndex(pieceIndex);
      }
    }
  }

  // Return the next piece with a swith.
  // If current piece has a switch it is not used.
  public int NextSwitch(int startPiece)
  {
    int currentPiece = startPiece + 1;
    if (currentPiece < 0) currentPiece = 0;
    if (currentPiece == this.pieces.Count) currentPiece = 0;

    while (currentPiece != startPiece)
    {
      if (this.pieces[currentPiece].switchPiece) return currentPiece;

      // Increasing the current piece
      currentPiece++;
      if (currentPiece == this.pieces.Count) currentPiece = 0;
    }

    return -1;
  }

  public string SwitchCommand(int currentPiece, int currentLane)
  {
    string switchCommand = null;
    int startSwitch = this.NextSwitch(currentPiece);
    int endSwitch = this.NextSwitch(startSwitch);

    if (startSwitch != -1 && endSwitch != -1 && startSwitch != endSwitch)
    {
      int shortestLaneIndex = -1;
      double shortestDistance = Double.MaxValue;

      for (int lane = 0; lane < this.lanes.Count; lane++)
      {
        if (this.LaneLength(lane, startSwitch + 1, endSwitch - 1) < shortestDistance)
        {
          shortestLaneIndex = lane;
          shortestDistance = this.LaneLength(lane, startSwitch + 1, endSwitch - 1);
        }
      }

      if (shortestLaneIndex != currentLane && currentLane >= 0 && currentLane < this.lanes.Count)
      {
        if (this.lanes[shortestLaneIndex].distanceFromCenter < this.lanes[currentLane].distanceFromCenter)
        {
          switchCommand = "Left";
        }
        else
        {
          switchCommand = "Right";
        }
      }
    }

    return switchCommand;
  }

  // Calculates the track lenght on given lane.
  // First piece and last piece are included in the length.
  public double LaneLength(int laneIndex, int firstPiece, int lastPiece)
  {
    double totalLength = 0.0;
    double laneOffset = 0.0;

    if (laneIndex >= 0 && laneIndex < this.lanes.Count)
    {
      laneOffset = this.lanes[laneIndex].distanceFromCenter;

      if (firstPiece < 0) firstPiece = 0;
      if (firstPiece >= this.pieces.Count) firstPiece = this.pieces.Count-1;

      if (lastPiece < 0) lastPiece = 0;
      if (lastPiece >= this.pieces.Count) lastPiece = this.pieces.Count - 1;

      totalLength += this.pieces[firstPiece].PieceLength(laneOffset);
      int currentPiece = this.NextPieceIndex(firstPiece);
      while (currentPiece != lastPiece && currentPiece != firstPiece)
      {
        if (currentPiece >= 0 && currentPiece < this.pieces.Count)
        {
          totalLength += this.pieces[currentPiece].PieceLength(laneOffset);
        }
        // Increasing the current piece
        currentPiece = this.NextPieceIndex(currentPiece);
      }
    }

    return totalLength;
  }

  public double ShortestLaneLength(int pieceIndex)
  {
    double shortestLength = Double.MaxValue;

    if (pieceIndex >= 0 && pieceIndex < this.pieces.Count)
    {
      foreach (TrackLane lane in this.lanes)
      {
        double length = this.pieces[pieceIndex].PieceLength(lane.distanceFromCenter);
        if (length < shortestLength)
        {
          shortestLength = length;
        }
      }
    }

    return shortestLength;
  }

  public void CreateSegments()
  {
    //Console.WriteLine("Creating segments");

    this.segments = new List<TrackSegment>();

    TrackSegment startSegment = new TrackSegment();
    int firstStartSegmentPiece = 0;
    while (startSegment.AddPiece(this.pieces[firstStartSegmentPiece], firstStartSegmentPiece))
    {
      firstStartSegmentPiece = this.PreviousPieceIndex(firstStartSegmentPiece);
    }

    firstStartSegmentPiece = this.NextPieceIndex(firstStartSegmentPiece);

    //Console.WriteLine("First segment start piece: " + firstStartSegmentPiece.ToString());

    int currentPieceIndex = firstStartSegmentPiece;
    TrackSegment currentSegment = new TrackSegment();
    do
    {
      //Console.WriteLine("Adding piece " + currentPieceIndex.ToString());
      if (!currentSegment.AddPiece(this.pieces[currentPieceIndex], currentPieceIndex))
      {
        //Console.WriteLine("Piece not belonging to previous segment, creating new one");
        this.segments.Add(currentSegment);
        currentSegment = new TrackSegment();
        currentSegment.AddPiece(this.pieces[currentPieceIndex], currentPieceIndex);
      }

      currentPieceIndex = this.NextPieceIndex(currentPieceIndex);
    } while (currentPieceIndex != firstStartSegmentPiece);

    this.segments.Add(currentSegment);

    //Console.WriteLine("Segments added");
  }

  public int GetSegmentIndexForPiece(int pieceIndex)
  {
    for (int i = 0; i < this.segments.Count; i++)
    {
      if (this.segments[i].pieceIndices.Contains(pieceIndex))
      {
        return i;
      }
    }

    return -1;
  }

  public TrackSegment GetSegmentForPiece(int pieceIndex)
  {
    foreach (TrackSegment segment in this.segments)
    {
      if (segment.pieceIndices.Contains(pieceIndex))
      {
        return segment;
      }
    }

    // Piece not found from segments.
    return null;
  }

  public bool IsLongestStraight(int pieceIndex)
  {
    TrackSegment longestStraight = null;
    foreach (TrackSegment segment in this.segments)
    {
      if (segment.IsStraight())
      {
        if (longestStraight == null || segment.totalLength > longestStraight.totalLength)
        {
          longestStraight = segment;
        }
      }
    }

    if (longestStraight != null)
    {
      if (longestStraight.pieceIndices.Contains(pieceIndex))
      {
        return true;
      }
    }

    return false;
  }

  public bool IsTightestLane(int piece, int lane)
  {
    bool isTightest = false;

    if (piece >= 0 && piece < this.pieces.Count)
    {
      if (this.pieces[piece].IsStraight())
      {
        isTightest = true;
      }
      else
      {
        double angle = this.pieces[piece].angle;

        if (angle < 0)
        {
          double smallestDistance = Double.MaxValue;
          foreach (TrackLane currLane in this.lanes)
          {
            if (currLane.distanceFromCenter < smallestDistance)
            {
              smallestDistance = currLane.distanceFromCenter;
            }
          }

          if (lane >= 0 && lane < this.lanes.Count)
          {
            if (this.lanes[lane].distanceFromCenter == smallestDistance)
            {
              isTightest = true;
            }
          }
        }
        else
        {
          double largestDistance = Double.MinValue;
          foreach (TrackLane currLane in this.lanes)
          {
            if (currLane.distanceFromCenter > largestDistance)
            {
              largestDistance = currLane.distanceFromCenter;
            }
          }

          if (lane >= 0 && lane < this.lanes.Count)
          {
            if (this.lanes[lane].distanceFromCenter == largestDistance)
            {
              isTightest = true;
            }
          }
        }
      }
    }

    return isTightest;
  }
}

public class TrackPiece
{
  public double length = 0.0;
  public double radius = 0.0;
  public double angle = 0.0;
  [JsonProperty("switch")]
  public bool switchPiece = false;

  public TrackPiece(double length, double radius, double angle, bool switchPiece)
  {
    this.length = length;
    this.radius = radius;
    this.angle = angle;
    this.switchPiece = switchPiece;
  }

  public bool IsStraight()
  {
    if (this.length > 0) return true;
    return false;
  }

  public double PieceLength(double laneOffset)
  {
    double laneLength = 0.0;

    if (this.IsStraight())
    {
      laneLength = this.length;
    }
    else
    {
      double laneRadius = this.LaneRadius(laneOffset);
      laneLength = Math.Abs(this.angle) * (Math.PI / 180.0) * laneRadius;
    }

    return laneLength;
  }

  public double LaneRadius(double laneOffset)
  {
    // The case for right hand turn.
    double laneRadius = this.radius - laneOffset;
    // The case for left hand turn.
    if (this.angle < 0) laneRadius = this.radius + laneOffset;

    return laneRadius;
  }
}

public class TrackLane
{
  public double distanceFromCenter = 0.0;
  public int index = 0;

  public TrackLane(double distanceFromCenter, int index)
  {
    this.distanceFromCenter = distanceFromCenter;
    this.index = index;
  }
}

public class TrackSegment
{
  public double totalLength = 0.0;
  public double minimumRadius = 0.0;
  public double totalAngle = 0.0;

  public List<int> pieceIndices = null;

  public TrackSegment()
  {
    this.pieceIndices = new List<int>();
  }

  public bool IsStraight()
  {
    if (this.totalLength > 0) return true;
    return false;
  }

  public bool AddPiece(TrackPiece piece, int pieceIndex)
  {
    bool validPieceToAdd = false;

    if (this.pieceIndices.Count == 0)
    {
      validPieceToAdd = true;
    }
    else if (piece.IsStraight() && this.IsStraight())
    {
      validPieceToAdd = true;
    }
    else if (!piece.IsStraight() && !this.IsStraight())
    {
      if (Math.Sign(piece.angle) == Math.Sign(this.totalAngle) &&
        piece.radius == this.minimumRadius)
      {
        validPieceToAdd = true;
      }
    }

    if (validPieceToAdd)
    {
      if (piece.IsStraight())
      {
        this.totalLength += piece.length;
      }
      else
      {
        if (Math.Abs(piece.radius) < Math.Abs(this.minimumRadius) || Math.Abs(this.minimumRadius) < Double.Epsilon)
        {
          this.minimumRadius = piece.radius;
        }
        this.totalAngle += piece.angle;
      }

      this.pieceIndices.Add(pieceIndex);
    }

    return validPieceToAdd;
  }
}
