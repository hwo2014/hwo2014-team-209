using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot
{
  // Uncomment this main to be used in the actual race
  public static void Main(string[] args)
  {
    string host = args[0];
    int port = int.Parse(args[1]);
    string botName = args[2];
    string botKey = args[3];

    Console.WriteLine("Version 1.2");

    Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

    using (TcpClient client = new TcpClient(host, port))
    {
      NetworkStream stream = client.GetStream();
      StreamReader reader = new StreamReader(stream);
      StreamWriter writer = new StreamWriter(stream);
      writer.AutoFlush = true;

      Bot racer = new Bot();
      racer.Start(reader, writer, new Join(botName, botKey));
    }
  }

  private double crashAngle = 60.0;

  private StreamReader reader = null;
  private StreamWriter writer = null;
  private SendMsg joinMessage = null;

  private string ownCarName = null;
  private string ownCarColor = null;

  private Track track = null;
  private List<RaceStatus> raceLog = null;

  // List of target speeds at different pieces of the track.
  private List<double> targetSpeeds = null;
  private List<double> crashSpeeds = null;

  private Dictionary<string, CarData> carData = null;

  private string alreadySwitching = null;
  private double lastThrottle = 0.0;
  private double lastSpeed = 0.0;

  private int lastCrashLap = -1;
  private int lastCrashPiece = -1;

  private string lastTrack = null;

  private bool braking = false;

  private bool iHaveCrashed = false;

  private bool turboAvailable = false;
  private int turboLength = 0;
  private double turboFactor = 0.0;
  private bool turboUsed = false;

  private string logDate = null;

  private int accelerationCount = 0;
  private int decelerationCount = 0;
  private List<double> decelerationApproximationSpeeds = null;
  private double decelerationConstant = 0.0;

  public Bot()
  {
  }

  public void Start(StreamReader reader, StreamWriter writer, SendMsg join, double throttle=-1.0)
  {
    this.logDate = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss ");
    System.IO.Directory.CreateDirectory("logs");

    // Telemetry header.
    string telemetryHeader = "Game tick;Lap;Piece index;Piece type;Piece length;Piece radius;Actual radius;Piece angle;";
    telemetryHeader += "Current piece distance;Start lane;End lane;Speed;Slip angle;New Throttle;Target speed";
    this.LogToTelemetry(telemetryHeader);

    this.raceLog = new List<RaceStatus>();
    this.carData = new Dictionary<string, CarData>();

    this.accelerationCount = 0;
    this.decelerationCount = 0;
    this.decelerationApproximationSpeeds = new List<double>();
    this.decelerationConstant = 0.0;

    this.reader = reader;
    this.writer = writer;
    this.joinMessage = join;

    this.Send(this.joinMessage);

    string line;
    while ((line = this.reader.ReadLine()) != null)
    {
      MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);

      this.LogToAllMessages("RECEIVED: " + line);
      this.LogToInput(line);
      
      switch (msg.msgType)
      {
        case "join":
          Console.WriteLine("Joined");
          break;
        case "yourCar":
          this.ProcessYourCar(msg);
          Console.WriteLine("Own car: " + this.ownCarName + " (" + this.ownCarColor + ")");
          break;
        case "gameInit":
          Console.WriteLine("Race init");
          this.ProcessGameInit(msg);
          break;
        case "gameStart":
          Console.WriteLine("Race starts");
          this.Send(new Throttle(1.0));
          break;
        case "carPositions":
          if (this.ProcessCarPositions(msg))
          {
            if (throttle < 0.0)
            {
              // Standard race logic
              this.DecideTickActions();
            }
            else
            {
              // Acceleration test / constant throttle race.
              this.DecideAccelerationTestActions(throttle);
            }
          }
          break;
        case "crash":
          this.ProcessCrash(msg);
          break;
        case "spawn":
          this.ProcessSpawn(msg);
          break;
        case "turboAvailable":
          this.ProcessTurbo(msg);
          break;
        case "lapFinished":
          this.ProcessLapFinished(msg);
          break;
        case "gameEnd":
          Console.WriteLine("Race ended");
          break;
        case "tournamentEnd":
          Console.WriteLine("Tournament ends");
          break;
        default:
          if (msg.data != null) Console.WriteLine(msg.msgType + ":\n" + msg.data.ToString());
          break;
      }

      this.LogTelemetry();
    } // While messages coming from the server

    this.LogCarData();
  }

  //
  // Message processors
  // 
  
  // Reading the data for my car.
  private void ProcessYourCar(MsgWrapper msg)
  {
    if (msg != null && msg.data != null)
    {
      Dictionary<string, string> carData = JsonConvert.DeserializeObject<Dictionary<string, string>>(msg.data.ToString());
      if (carData.ContainsKey("name")) this.ownCarName = carData["name"];
      if (carData.ContainsKey("color")) this.ownCarColor = carData["color"];
    }
  }

  // Read the track data and create the initial target speeds.
  private void ProcessGameInit(MsgWrapper msg)
  {
    if (msg != null && msg.data != null)
    {
      Dictionary<string, dynamic> race = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(msg.data.ToString());
      if (race.ContainsKey("race"))
      {
        Dictionary<string, dynamic> raceObjects = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(race["race"].ToString());

        if (raceObjects.ContainsKey("track"))
        {
          this.track = JsonConvert.DeserializeObject<Track>(raceObjects["track"].ToString());
          //System.IO.File.WriteAllText("track.csv", this.track.ToString());

          if (this.lastTrack == null || this.lastTrack != this.track.id)
          {
            this.targetSpeeds = new List<double>();
            this.crashSpeeds = new List<double>();
            foreach (TrackPiece piece in this.track.pieces)
            {
              this.targetSpeeds.Add(this.CalculatePieceTargetSpeed(piece));
              this.crashSpeeds.Add(Double.MaxValue);
            }
            this.lastTrack = this.track.id;
          }
        }
      }
    }

    this.SaveTargetSpeeds(-1);
  }

  // Add next race status to race log
  // and store the car data for all cars.
  // Returns if there should be a response to the message (contains gameTick-field).
  private bool ProcessCarPositions(MsgWrapper msg)
  {
    if (msg != null && msg.data != null)
    {
      int gameTick = -1;
      if (msg.gameTick != null)
      {
        try
        {
          gameTick = int.Parse(msg.gameTick);
        }
        catch (Exception ex)
        {
          Console.WriteLine("Can't parse game tick");
          Console.WriteLine(ex.Message);
        }
      }
      
      List<Car> cars = JsonConvert.DeserializeObject<List<Car>>(msg.data.ToString());
      if (this.raceLog != null) this.raceLog.Add(new RaceStatus(cars, msg.gameId, gameTick));

      this.CollectAndProcessCarData();

      if (gameTick >= 0)
      {
        return true;
      }
    }

    return false;
  }

  private void ProcessCrash(MsgWrapper msg)
  {
    if (msg != null & msg.data != null)
    {
      Dictionary<string, string> data = JsonConvert.DeserializeObject<Dictionary<string, string>>(msg.data.ToString());

      if (data.ContainsKey("color") && data["color"] == this.ownCarColor &&
        this.raceLog.Count > 0)
      {
        Console.WriteLine("Crashed");

        RaceStatus raceStatus = this.raceLog[this.raceLog.Count - 1];
        Car ownCar = raceStatus.GetCar(this.ownCarColor);

        string crashMessage = "";

        if (ownCar != null)
        {
          crashMessage = "lap;" + ownCar.position.lap.ToString() + ";";
          crashMessage += "piece;" + ownCar.position.pieceIndex.ToString() + ";";
          crashMessage += "speed;" + this.CurrentSpeed().ToString("F3") + ";";
          crashMessage += "angle;" + ownCar.angle.ToString("F3") + ";";

          int pieceIndex = ownCar.position.pieceIndex;
          // Adjusting only the cornering speeds.
          while (this.track.pieces[pieceIndex].IsStraight())
          {
            pieceIndex = this.track.PreviousPieceIndex(pieceIndex);
          }

          TrackSegment currentSegment = this.track.GetSegmentForPiece(pieceIndex);
          if (currentSegment != null)
          {
            foreach (int piece in currentSegment.pieceIndices)
            {
              if (this.targetSpeeds != null && piece >= 0 && piece < this.targetSpeeds.Count)
              {
                crashMessage += "piece;" + piece + ";old target;" + this.targetSpeeds[piece].ToString("F3") + ";";
                this.targetSpeeds[piece] = 0.9 * this.CurrentSpeed();
                crashMessage += "piece;" + piece + ";new target;" + this.targetSpeeds[piece].ToString("F3") + ";";
              }

              if (this.crashSpeeds != null && piece >= 0 && piece < this.crashSpeeds.Count)
              {
                if (this.CurrentSpeed() < this.crashSpeeds[piece])
                {
                  this.crashSpeeds[piece] = this.CurrentSpeed();
                }
              }
            }
          }

          this.lastCrashLap = ownCar.position.lap;
          this.lastCrashPiece = ownCar.position.pieceIndex;

          this.Log("crash", crashMessage);
        }

        this.iHaveCrashed = true;
      }
    }
  }

  private void ProcessSpawn(MsgWrapper msg)
  {
    if (msg != null & msg.data != null)
    {
      Dictionary<string, string> data = JsonConvert.DeserializeObject<Dictionary<string, string>>(msg.data.ToString());

      if (data.ContainsKey("color") && data["color"] == this.ownCarColor)
      {
        this.iHaveCrashed = false;
        // When returning to race we have lost the turbo.
        this.turboAvailable = false;
      }
    }
  }

  private void ProcessTurbo(MsgWrapper msg)
  {
    // Turbo messages when crashed should be ignored.
    if (!this.iHaveCrashed)
    {
      if (msg != null & msg.data != null)
      {
        Dictionary<string, dynamic> data = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(msg.data.ToString());

        if (data.ContainsKey("turboDurationTicks") &&
          data.ContainsKey("turboFactor"))
        {
          try
          {
            this.turboAvailable = true;
            this.turboLength = int.Parse(data["turboDurationTicks"].ToString());
            this.turboFactor = double.Parse(data["turboFactor"].ToString());
          }
          catch (Exception ex)
          {
            Console.WriteLine(ex.Message);
          }
        }
      }
    }
  }

  private void ProcessLapFinished(MsgWrapper msg)
  {
    if (msg != null & msg.data != null)
    {
      Dictionary<string, dynamic> data = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(msg.data.ToString());
      int lap = -1;
      string car = "";
      double lapTime = 0.0;

      if (data.ContainsKey("lapTime"))
      {
        Dictionary<string, dynamic> lapTimeInfo = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(data["lapTime"].ToString());
        if (lapTimeInfo.ContainsKey("lap"))
        {
          try
          {
            lap = int.Parse(lapTimeInfo["lap"].ToString());
          }
          catch (Exception ex)
          {
            Console.WriteLine(ex.Message);
          }
        }

        if (lapTimeInfo.ContainsKey("millis"))
        {
          try
          {
            int millis = int.Parse(lapTimeInfo["millis"].ToString());
            lapTime = millis / 1000.0;
          }
          catch (Exception ex)
          {
            Console.WriteLine(ex.Message);
          }
        }
      }

      if (data.ContainsKey("car"))
      {
        Dictionary<string, string> finishedCar = JsonConvert.DeserializeObject<Dictionary<string, string>>(data["car"].ToString());
        if (finishedCar.ContainsKey("color"))
        {
          if (finishedCar["color"] == this.ownCarColor)
          {
            this.SaveTargetSpeeds(lap);
          }
          car = finishedCar["color"];
        }
      }

      Console.WriteLine(car + " finished lap " + lap.ToString() + ": lap time " + lapTime.ToString("F3"));
    }
  }

  //
  // Helper functions
  // 

  private double CalculatePieceTargetSpeed(TrackPiece piece)
  {
    // Simple logic based on straight and curves:
    if (piece.IsStraight())
      return 100.0;
    else
      return 3.0;

    // More "advanced" logic with curve radius:
    //double targetSpeed = 0.0;
    //if (piece.IsStraight())
    //{
    //  targetSpeed = 100.0;
    //}
    //else
    //{
    //  if (piece.radius < 60)
    //    targetSpeed = 4.0;
    //  else if (piece.radius < 90)
    //    targetSpeed = 5.0;
    //  else if (piece.radius < 120)
    //    targetSpeed = 6.5;
    //  else if (piece.radius < 220)
    //    targetSpeed = 9.2;
    //  else
    //    targetSpeed = 10.0;
    //}

    //return targetSpeed;
  }

  private void SaveTargetSpeeds(int lap)
  {
    string header = "Lap;Piece;Target speed";
    this.Log("target-speeds", header);

    for (int i = 0; i < this.targetSpeeds.Count; i++)
    {
      string logMessage = lap.ToString() + ";";
      logMessage += i.ToString() + ";";
      logMessage += this.targetSpeeds[i].ToString("F3");
      this.Log("target-speeds", logMessage);
    }

    this.SaveCrashSpeeds(lap);
  }

  private void SaveCrashSpeeds(int lap)
  {
    string header = "Lap;Piece;Target speed";
    this.Log("crash-speeds", header);

    for (int i = 0; i < this.crashSpeeds.Count; i++)
    {
      string logMessage = lap.ToString() + ";";
      logMessage += i.ToString() + ";";
      if (this.crashSpeeds[i] < Double.MaxValue)
      {
        logMessage += this.crashSpeeds[i].ToString("F3");
      }
      else
      {
        logMessage += "---";
      }
      this.Log("crash-speeds", logMessage);
    }
  }

  // Store the data for all the cars in the race for this car positions.
  // Adjusts also the target speeds if there is need based on the data.
  private void CollectAndProcessCarData()
  {
    if (this.raceLog != null && this.raceLog.Count > 1)
    {
      RaceStatus currentStatus = this.raceLog[this.raceLog.Count - 1];
      RaceStatus previousStatus = this.raceLog[this.raceLog.Count - 2];

      foreach (Car currentCar in currentStatus.cars)
      {
        Car previousCar = previousStatus.GetCar(currentCar.Color());

        int pieceIndex = previousCar.position.pieceIndex;
        int lap = previousCar.position.lap;
        int laneIndex = previousCar.position.lane.endLaneIndex;

        double pieceDistance = previousCar.position.inPieceDistance;
        double speed = currentCar.GetSpeed(previousCar.position, this.track,
          currentStatus.gameTick, previousStatus.gameTick);
        double angle = currentCar.angle;

        CarData currentData = new CarData();
        if (this.carData.ContainsKey(currentCar.Color()))
        {
          currentData = this.carData[currentCar.Color()];
        }

        PieceData pieceData = new PieceData();
        if (currentData.pieceData.ContainsKey(pieceIndex))
        {
          pieceData = currentData.pieceData[pieceIndex];
        }

        LaneData laneData = new LaneData();
        if (pieceData.laneData.ContainsKey(lap))
        {
          if (pieceData.laneData[lap].ContainsKey(laneIndex))
          {
            laneData = pieceData.laneData[lap][laneIndex];
          }
        }

        SpeedData currentSpeedData = new SpeedData(pieceDistance, speed, angle);
        laneData.speedData.Add(currentSpeedData);

        if (!pieceData.laneData.ContainsKey(lap))
        {
          pieceData.laneData[lap] = new Dictionary<int, LaneData>();
        }
        pieceData.laneData[lap][laneIndex] = laneData;
        currentData.pieceData[pieceIndex] = pieceData;
        this.carData[currentCar.Color()] = currentData;

        bool pieceWiseTargetSpeeds = false;
        if (pieceWiseTargetSpeeds)
        {
          // Checking if target speeds need adjusting
          if (currentCar.position.pieceIndex != previousCar.position.pieceIndex)
          {
            this.AdjustPieceTargetSpeed(previousCar.Color(), pieceIndex, lap, laneIndex);
          }
        }
        else // Segment-wise target speeds
        {
          // Checking if target speeds need adjusting
          if (currentCar.position.pieceIndex != previousCar.position.pieceIndex &&
            this.track.GetSegmentIndexForPiece(currentCar.position.pieceIndex) !=
            this.track.GetSegmentIndexForPiece(previousCar.position.pieceIndex))
          {
            this.AdjustSegmentTargetSpeed(previousCar.Color(), pieceIndex, lap, laneIndex);
          }
        }

        if (currentCar.Color() == this.ownCarColor)
        {
          if (this.decelerationCount > 0)
          {
            this.decelerationApproximationSpeeds.Add(speed);
            if (this.decelerationApproximationSpeeds.Count > 3 &&
              this.decelerationApproximationSpeeds.Count < 5)
            {
              this.CalculateDecelerationConstant();
            }
          }
        }

        // Quick hack for turbo used: mark it used in the first corner -> normalize braking distances.
        if (currentCar.Color() == this.ownCarColor)
        {
          if (currentCar.position.pieceIndex != previousCar.position.pieceIndex &&
            this.track.GetSegmentIndexForPiece(currentCar.position.pieceIndex) !=
            this.track.GetSegmentIndexForPiece(previousCar.position.pieceIndex))
          {
            int currentSegment = this.track.GetSegmentIndexForPiece(currentCar.position.pieceIndex);
            if (currentSegment >= 0 && currentSegment < this.track.segments.Count)
            {
              if (!this.track.segments[currentSegment].IsStraight())
              {
                this.turboUsed = false;
              }
            }
          }
        }
      }
    }
  }

  // Calculate and adjust new target speed piece-wise (as opposed to segment-wise).
  private void AdjustPieceTargetSpeed(string car, int pieceIndex, int lap, int lane)
  {
    //if (this.carData.ContainsKey(car))
    //{
    //  CarData currentCar = this.carData[car];
    //  bool isOwnCar = false;
    //  if (car == this.ownCarColor)
    //  {
    //    isOwnCar = true;
    //  }

    //  if (currentCar.pieceData.ContainsKey(pieceIndex))
    //  {
    //    PieceData currentPiece = currentCar.pieceData[pieceIndex];

    //    if (currentPiece.laneData.ContainsKey(lap))
    //    {
    //      if (currentPiece.laneData[lap].ContainsKey(lane))
    //      {
    //        string logMessage = "Calculating speed data for car " + car;
    //        logMessage += ", piece " + pieceIndex.ToString();
    //        logMessage += ", lap " + lap.ToString();
    //        logMessage += ", lane " + lane.ToString() + "\n";

    //        LaneData currentLane = currentPiece.laneData[lap][lane];

    //        double minimumSpeed = Double.MaxValue;
    //        double maximumAngle = 0.0;

    //        foreach (SpeedData speedData in currentLane.speedData)
    //        {
    //          if (speedData.speed < minimumSpeed)
    //          {
    //            minimumSpeed = speedData.speed;
    //          }

    //          if (Math.Abs(speedData.slipAngle) > maximumAngle)
    //          {
    //            maximumAngle = Math.Abs(speedData.slipAngle);
    //          }
    //        }

    //        logMessage += "  Min speed: " + minimumSpeed.ToString("F3");
    //        logMessage += ", max angle: " + maximumAngle.ToString("F3") + "\n";

    //        if (this.targetSpeeds.Count > pieceIndex)
    //        {
    //          logMessage += "  Old target speed: " + this.targetSpeeds[pieceIndex].ToString("F3") + "\n";
    //        }

    //        this.CalculateNewTargetSpeed(pieceIndex, minimumSpeed, maximumAngle, isOwnCar);

    //        if (this.targetSpeeds.Count > pieceIndex)
    //        {
    //          logMessage += "  New target speed: " + this.targetSpeeds[pieceIndex].ToString("F3") + "\n";
    //        }

    //        this.Log("target-speed-adjustments", logMessage);
    //      }
    //    }
    //  }
    //}
  }

  private void AdjustSegmentTargetSpeed(string car, int pieceIndex, int lap, int lane)
  {
    if (this.carData.ContainsKey(car))
    {
      CarData currentCar = this.carData[car];
      bool isOwnCar = false;
      if (car == this.ownCarColor)
      {
        isOwnCar = true;
      }

      TrackSegment currentSegment = this.track.GetSegmentForPiece(pieceIndex);
      if (currentSegment != null)
      {
        List<int> segmentPieces = currentSegment.pieceIndices;

        // Checks crashes only from own car.
        bool crashOnSegment = false;
        if (this.lastCrashLap == lap && segmentPieces.Contains(this.lastCrashPiece) && isOwnCar)
        {
          crashOnSegment = true;
        }

        // Adjust speeds only if there hasn't been a crash on the segment earlier.
        if (!crashOnSegment)
        {
          double minimumSpeed = Double.MaxValue;
          double maximumAngle = 0.0;

          Dictionary<int, double> pieceMinimumSpeeds = new Dictionary<int,double>();
          Dictionary<int, double> pieceMaximumAngles = new Dictionary<int, double>();

          string logMessage = "car;" + car + ";";
          logMessage += "piece;" + pieceIndex.ToString() + ";";
          logMessage += "lap;" + lap.ToString() + ";";
          logMessage += "lane;" + lane.ToString() + ";";

          foreach (int segmentPiece in segmentPieces)
          {
            if (currentCar.pieceData.ContainsKey(segmentPiece))
            {
              PieceData currentPiece = currentCar.pieceData[segmentPiece];

              if (currentPiece.laneData.ContainsKey(lap))
              {
                if (currentPiece.laneData[lap].ContainsKey(lane))
                {
                  LaneData currentLane = currentPiece.laneData[lap][lane];

                  foreach (SpeedData speedData in currentLane.speedData)
                  {
                    if (speedData.speed < minimumSpeed)
                    {
                      minimumSpeed = speedData.speed;
                    }

                    if (Math.Abs(speedData.slipAngle) > maximumAngle)
                    {
                      maximumAngle = Math.Abs(speedData.slipAngle);
                    }

                    if (!pieceMinimumSpeeds.ContainsKey(segmentPiece) ||
                      speedData.speed < pieceMinimumSpeeds[segmentPiece])
                    {
                      pieceMinimumSpeeds[segmentPiece] = speedData.speed;
                    }

                    if (!pieceMaximumAngles.ContainsKey(segmentPiece) || 
                      Math.Abs(speedData.slipAngle) > pieceMaximumAngles[segmentPiece])
                    {
                      pieceMaximumAngles[segmentPiece] = Math.Abs(speedData.slipAngle);
                    }
                  }
                }
              }
            }
          }

          logMessage += "min speed;" + minimumSpeed.ToString("F3") + ";";
          logMessage += "max angle;" + maximumAngle.ToString("F3") + ";";

          foreach (int segmentPiece in segmentPieces)
          {
            if (this.targetSpeeds.Count > segmentPiece)
            {
              logMessage += "piece;" + segmentPiece + ";old target;" + this.targetSpeeds[segmentPiece].ToString("F3") + ";";
            }

            if (isOwnCar)
            {
              this.CalculateNewTargetSpeed(segmentPiece, lane, minimumSpeed, maximumAngle, isOwnCar);
            }
            else if (pieceMinimumSpeeds.ContainsKey(segmentPiece) && pieceMaximumAngles.ContainsKey(segmentPiece))
            {
              this.CalculateNewTargetSpeed(segmentPiece, lane, pieceMinimumSpeeds[segmentPiece], pieceMaximumAngles[segmentPiece], isOwnCar);
            }
            else
            {
              this.CalculateNewTargetSpeed(segmentPiece, lane, minimumSpeed, maximumAngle, isOwnCar);
            }

            if (this.targetSpeeds.Count > segmentPiece)
            {
              logMessage += "new target;" + this.targetSpeeds[segmentPiece].ToString("F3") + ";";
            }
          }

          this.Log("target-speed-adjustments", logMessage);
        }
      }
    }
  }

  // Calculates and saves the new target speed for the piece.
  private double CalculateNewTargetSpeed(int pieceIndex, int lane, double minimumSpeed, double maximumAngle, bool isOwnCar)
  {
    double maxAngleRelativeToCrash = maximumAngle / this.crashAngle;
    double newTargetSpeed = 0.0;
    double newCrashSpeed = -1.0;

    if (maxAngleRelativeToCrash < 0.8)
    {
      if (this.targetSpeeds.Count > pieceIndex &&
        this.crashSpeeds.Count > pieceIndex)
      {
        if (isOwnCar)
        {
          if (maximumAngle < 5.0)
          {
            newTargetSpeed = 1.3 * minimumSpeed;
          }
          else if (maximumAngle > this.crashAngle - 5.0)
          {
            newCrashSpeed = 1.1 * minimumSpeed;
          }
          else
          {
            //if (this.track.IsTightestLane(pieceIndex, lane))
            //{
              newTargetSpeed = (1.1 - (maxAngleRelativeToCrash / 8.0)) * minimumSpeed;
            //}
            //else
            //{
            //  newTargetSpeed = (1.05 - (maxAngleRelativeToCrash / 8.0)) * minimumSpeed;
            //}
          }
        }
        else
        {
          if (this.track.IsTightestLane(pieceIndex, lane))
          {
            newTargetSpeed = minimumSpeed;
          }
        }

        if (newCrashSpeed > 0.0)
        {
          this.crashSpeeds[pieceIndex] = newCrashSpeed;
          this.targetSpeeds[pieceIndex] = 0.9 * newCrashSpeed;
        }
        else
        {
          if (newTargetSpeed >= 0.9 * this.crashSpeeds[pieceIndex])
          {
            newTargetSpeed = 0.9 * this.crashSpeeds[pieceIndex];
          }

          if (newTargetSpeed > this.targetSpeeds[pieceIndex])
          {
            this.targetSpeeds[pieceIndex] = newTargetSpeed;
          }
        }
      }
    }

    return newTargetSpeed;
  }

  private void CalculateDecelerationConstant()
  {
    Console.WriteLine("Calculating deceleration constant");

    if (this.decelerationApproximationSpeeds.Count > 3)
    {
      double speedDifference01 = this.decelerationApproximationSpeeds[1] - this.decelerationApproximationSpeeds[0];
      double speedDifference12 = this.decelerationApproximationSpeeds[2] - this.decelerationApproximationSpeeds[1];
      double speedDifference23 = this.decelerationApproximationSpeeds[3] - this.decelerationApproximationSpeeds[2];

      double constant1 = Math.Log(speedDifference12 / speedDifference01);
      double constant2 = Math.Log(speedDifference23 / speedDifference12);

      Console.WriteLine("Constants: " + constant1.ToString("F6") + " - " + constant2.ToString("F6"));

      this.decelerationConstant = constant1;
    }
  }

  // Decides what to send on this car positions tick.
  private void DecideTickActions()
  {
    string switchCommand = this.DecideToSwitch();
    bool fireTurbo = this.DecideToTurbo();
    double newThrottle = this.DecideThrottle();

    if (newThrottle != this.lastThrottle)
    {
      this.Send(new Throttle(newThrottle));
    }
    else if (switchCommand != null)
    {
      this.Send(new SwitchLane(switchCommand));
    }
    else if (fireTurbo)
    {
      this.turboAvailable = false;
      this.turboUsed = true;
      this.Send(new Turbo("GO GO GO!"));
    }
    else
    {
      this.Send(new Throttle(newThrottle));
    }
  }

  // Should we send switch message.
  private string DecideToSwitch()
  {
    string switchCommand = null;

    if (this.raceLog.Count > 0)
    {
      RaceStatus currentStatus = this.raceLog[this.raceLog.Count - 1];
      Car ownCar = currentStatus.GetCar(this.ownCarColor);

      if (ownCar != null)
      {
        int currentPiece = ownCar.position.pieceIndex;

        if (this.track.NextPiece(currentPiece).switchPiece)
        {
          //int currentLane = ownCar.position.lane.endLaneIndex;
          //switchCommand = this.track.SwitchCommand(currentPiece, currentLane);
          Random rnd = new Random();
          int newLane = rnd.Next(0, 2);
          if (newLane == 0)
            switchCommand = "Right";
          else
            switchCommand = "Left";
        }
      }
    }

    if (switchCommand == this.alreadySwitching) switchCommand = null;

    return switchCommand;
  }

  // Should the turbo be fired.
  private bool DecideToTurbo()
  {
    if (this.turboAvailable)
    {
      if (this.raceLog.Count > 0)
      {
        RaceStatus currentStatus = this.raceLog[this.raceLog.Count - 1];
        Car ownCar = currentStatus.GetCar(this.ownCarColor);

        if (ownCar != null)
        {
          int currentPiece = ownCar.position.pieceIndex;
          if (this.track.IsLongestStraight(currentPiece))
          {
            return true;
          }
        }
      }
    }

    return false;
  }

  // Which throttle value to send on this tick.
  private double DecideThrottle()
  {
    if (this.accelerationCount > 5 && this.decelerationCount < 5)
    {
      this.decelerationCount++;
      return 0.0;
    }

    if (this.raceLog.Count > 0)
    {
      RaceStatus currentStatus = this.raceLog[this.raceLog.Count - 1];
      Car ownCar = currentStatus.GetCar(this.ownCarColor);

      if (ownCar != null)
      {
        double throttle = 1.0;

        List<PositionSpeed> speeds =
          this.CalculatePositionSpeeds(this.CurrentSpeed(ownCarColor), ownCar.position.pieceIndex, ownCar.position.inPieceDistance);

        //Console.WriteLine("Speeds count: " + speeds.Count.ToString());
        //foreach (PositionSpeed posSpeed in speeds)
        //{
        //  this.Log("pos-speeds", posSpeed.pieceIndex.ToString() + ";" + posSpeed.pieceDistance.ToString() + ";" + posSpeed.speed.ToString());
        //}

        int tickMargin = 5;
        for (int i = 0; i < speeds.Count; i++)
        {
          int currentPiece = speeds[i].pieceIndex;
          if (currentPiece >= 0 && currentPiece < this.targetSpeeds.Count)
          {
            for (int j = 0; j <= tickMargin; j++)
            {
              if (i - j >= 0)
              {
                double safetyMargin = 1.0;
                // If we are braking, it requires some margin before we start accelerating again.
                if (this.lastThrottle < 0.05)
                {
                  safetyMargin = 0.9;
                }

                if (speeds[i - j].speed > this.targetSpeeds[currentPiece])
                {
                  throttle = 0.0;
                  break;
                }
                else if (speeds[i - j].speed > this.targetSpeeds[currentPiece] * safetyMargin &&
                  throttle > this.targetSpeeds[currentPiece] / 10.0)
                {
                  throttle = this.targetSpeeds[currentPiece] / 10.0;
                }
              }
            }
          }
        }

        //int piecesForward = 2;
        //if (this.turboUsed)
        //{
        //  piecesForward = 6;
        //}

        //int currentPiece = ownCar.position.pieceIndex;
        //for (int i = 0; i <= piecesForward; i++)
        //{
        //  if (this.CurrentSpeed() > this.targetSpeeds[currentPiece])
        //  {
        //    throttle = 0.0;
        //    break;
        //  }

        //  currentPiece = this.track.NextPieceIndex(currentPiece);
        //}

        return throttle;
      }
    }

    return 0.5;
  }

  private List<PositionSpeed> CalculatePositionSpeeds(double currentSpeed, int startPieceIndex, double startPieceDistance)
  {
    List<PositionSpeed> speeds = new List<PositionSpeed>();

    if (this.decelerationCount > 3)
    {
      int pieceIndex = startPieceIndex;
      double pieceDistance = startPieceDistance;
      double speed = currentSpeed;

      speeds.Add(new PositionSpeed(pieceIndex, pieceDistance, speed));

      while (speed > 0.001 && speeds.Count < 10000)
      {
        this.track.DriveForward(ref pieceIndex, ref pieceDistance, speed);

        speed = speed * Math.Exp(this.decelerationConstant);

        speeds.Add(new PositionSpeed(pieceIndex, pieceDistance, speed));
      }
    }

    return speeds;
  }

  private double CurrentSpeed(string carColor = null)
  {
    if (carColor == null)
    {
      carColor = this.ownCarColor;
    }

    double speed = -1.0;
    int currentStatusIndex = this.raceLog.Count-1;

    while (speed < 0.0 && currentStatusIndex > 0)
    {
      RaceStatus previousStatus = this.raceLog[currentStatusIndex - 1];
      RaceStatus currentStatus = this.raceLog[currentStatusIndex];

      Car previousCar = previousStatus.GetCar(carColor);
      Car currentCar = currentStatus.GetCar(carColor);

      if (previousCar != null & currentCar != null)
      {
        speed = currentCar.GetSpeed(previousCar.position, this.track, currentStatus.gameTick, previousStatus.gameTick);
      }

      currentStatusIndex--;
    }

    return speed;
  }

  //
  // Other functions, but why?
  // 

  // Function just for the acceleration / braking / constant throttle testing...
  private void DecideAccelerationTestActions(double throttle)
  {
    // Decide if we need to break or accelerate:
    if (this.CurrentSpeed() < 0.0001)
    {
      this.braking = false;
    }
    else if (this.IsOnFirstPieceInMainStraight())
    {
      this.braking = true;
    }
    else if (Math.Abs(this.CurrentSpeed() - this.lastSpeed) < 0.000001)
    {
      this.braking = false;
    }

    this.lastSpeed = this.CurrentSpeed();

    // No braking test now...
    this.braking = false;

    // Decide the braking power:
    if (this.braking)
    {
      double newThrottle = 0.0;

      // Break less in the next laps:
      if (this.CurrentLap() == 1)
      {
        newThrottle = throttle / 2.0;
      }
      else if (this.CurrentLap() == 2)
      {
        newThrottle = throttle / 3.0;
      }

      // On the first piece use the calculated throttle,
      // after that we can't be sure if we are on the same lap or already
      // on the next lap: so we use the last throttle setting.
      if (this.IsOnFirstPieceInMainStraight())
      {
        this.Send(new Throttle(newThrottle));
      }
      else
      {
        this.Send(new Throttle(this.lastThrottle));
      }
    }
    else
    {
      this.Send(new Throttle(throttle));
    }
  }

  private int CurrentLap()
  {
    int lap = -1;

    if (this.raceLog.Count > 0)
    {
      RaceStatus currentStatus = this.raceLog[this.raceLog.Count - 1];
      Car currentOwnCar = currentStatus.GetCar(this.ownCarColor);

      if (currentOwnCar != null)
      {
        lap = currentOwnCar.position.lap;
      }
    }

    return lap;
  }

  private bool IsOnFirstPieceInMainStraight()
  {
    bool onFirstPieceInMainStraight = false;

    int firstPieceIndexMainStraight = -1;

    if (this.track != null)
    {
      for (int piece = this.track.pieces.Count - 1; piece >= 0; piece--)
      {
        if (this.track.pieces[piece].IsStraight())
        {
          firstPieceIndexMainStraight = piece;
        }
        else
        {
          break;
        }
      }
    }

    if (this.raceLog.Count > 0)
    {
      RaceStatus currentStatus = this.raceLog[this.raceLog.Count - 1];
      Car currentOwnCar = currentStatus.GetCar(this.ownCarColor);

      if (currentOwnCar != null)
      {
        if (currentOwnCar.position.pieceIndex == firstPieceIndexMainStraight)
        {
          onFirstPieceInMainStraight = true;
        }
      }
    }

    return onFirstPieceInMainStraight;
  }

  //
  // Logging
  // 

  private void LogTelemetry()
  {
    int gameTick = 0;
    int lap = 0;

    int pieceIndex = 0;
    string pieceType = "straight";
    double pieceLength = 0;
    double pieceRadius = 0;
    double actualRadius = 0;
    double pieceAngle = 0;

    double currentPieceDistance = 0.0;
    
    int startLane = 0;
    int endLane = 0;

    double speed = 0.0;
    double slipAngle = 0.0;
    double targetSpeed = 0.0;

    if (this.raceLog.Count > 1)
    {
      RaceStatus previousStatus = this.raceLog[this.raceLog.Count - 2];
      RaceStatus currentStatus = this.raceLog[this.raceLog.Count - 1];

      Car previousOwnCar = previousStatus.GetCar(this.ownCarColor);
      Car currentOwnCar = currentStatus.GetCar(this.ownCarColor);

      if (previousOwnCar != null & currentOwnCar != null)
      {
        speed = this.CurrentSpeed(this.ownCarColor);
        //currentOwnCar.GetSpeed(previousOwnCar.position, this.track, currentStatus.gameTick, previousStatus.gameTick);
      }

      gameTick = currentStatus.gameTick;
      lap = currentOwnCar.position.lap;
      
      pieceIndex = currentOwnCar.position.pieceIndex;
      if (!track.pieces[pieceIndex].IsStraight()) pieceType = "curve";
      pieceLength = track.pieces[pieceIndex].length;
      pieceRadius = track.pieces[pieceIndex].radius;

      if (!track.pieces[pieceIndex].IsStraight())
      {
        double laneOffset = this.track.lanes[currentOwnCar.position.lane.endLaneIndex].distanceFromCenter;
        actualRadius = track.pieces[pieceIndex].LaneRadius(laneOffset);
      }

      pieceAngle = track.pieces[pieceIndex].angle;

      currentPieceDistance = currentOwnCar.position.inPieceDistance;

      startLane = currentOwnCar.position.lane.startLaneIndex;
      endLane = currentOwnCar.position.lane.endLaneIndex;

      slipAngle = currentOwnCar.angle;

      if (this.targetSpeeds.Count > pieceIndex)
      {
        targetSpeed = this.targetSpeeds[pieceIndex];
      }
    }

    string logLine = "";
    logLine += gameTick.ToString() + ";";
    logLine += lap.ToString() + ";";

    logLine += pieceIndex.ToString() + ";";
    logLine += pieceType.ToString() + ";";
    logLine += pieceLength.ToString() + ";";
    logLine += pieceRadius.ToString() + ";";
    logLine += actualRadius.ToString() + ";";
    logLine += pieceAngle.ToString() + ";";

    logLine += currentPieceDistance.ToString() + ";";

    logLine += startLane.ToString() + ";";
    logLine += endLane.ToString() + ";";

    logLine += speed.ToString() + ";";
    logLine += slipAngle.ToString() + ";";
    logLine += this.lastThrottle.ToString() + ";";
    logLine += targetSpeed.ToString();

    this.LogToTelemetry(logLine);
  }

  /// <summary> Logs car data. </summary>
  private void LogCarData()
  {
    string header = "Car;Piece;Lap;Lane;Start distance;Speed;Angle";
    this.Log("car-data", header);

    foreach (KeyValuePair<string, CarData> data in this.carData)
    {
      foreach (KeyValuePair<int, PieceData> pieceData in data.Value.pieceData)
      {
        foreach (KeyValuePair<int, Dictionary<int, LaneData>> lapData in pieceData.Value.laneData)
        {
          foreach (KeyValuePair<int, LaneData> laneData in lapData.Value)
          {
            foreach (SpeedData speedData in laneData.Value.speedData)
            {
              string logMessage = data.Key + ";";
              logMessage += pieceData.Key.ToString() + ";";
              logMessage += lapData.Key.ToString() + ";";
              logMessage += laneData.Key.ToString() + ";";
              logMessage += speedData.pieceDistance.ToString("F6") + ";";
              logMessage += speedData.speed.ToString("F6") + ";";
              logMessage += speedData.slipAngle.ToString("F6");
              this.Log("car-data", logMessage);
            }
          }
        }
      }
    }
  }

  private void Send(SendMsg msg)
  {
    this.writer.WriteLine(msg.ToJson());
    this.LogToAllMessages("SENT: " + msg.ToJson());
    this.LogToOutput(msg.ToJson());

    if (msg.MsgType() == "throttle")
    {
      this.lastThrottle = ((Throttle)msg).value;
      if (this.lastThrottle > 0.0)
      {
        this.accelerationCount++;
      }
    }
    else if (msg.MsgType() == "switchLane")
    {
      this.alreadySwitching = msg.MsgData().ToString();
    }
  }

  private void LogToAllMessages(string msg)
  {
    //this.Log("messages", msg);
  }

  private void LogToInput(string msg)
  {
    //this.Log("input", msg);
  }

  private void LogToOutput(string msg)
  {
    //this.Log("output", msg);
  }

  private void LogToTelemetry(string msg)
  {
    this.Log("telemetry", msg);
  }

  private void Log(string fileBase, string msg)
  {
    //StreamWriter logWriter = new StreamWriter("logs/" + this.logDate + fileBase + ".log", true);
    //logWriter.WriteLine(msg);
    //logWriter.Close();
    Console.WriteLine(fileBase + "#" + msg);
  }
}

public class MsgWrapper
{
  public string msgType = null;
  public Object data = null;
  public string gameId = null;
  public string gameTick = null;

  public MsgWrapper(string msgType, Object data)
  {
    this.msgType = msgType;
    this.data = data;
  }

  [JsonConstructor]
  public MsgWrapper(string msgType, Object data, string gameId, string gameTick)
  {
    this.msgType = msgType;
    this.data = data;
    this.gameId = gameId;
    this.gameTick = gameTick;
  }
}

public abstract class SendMsg
{
  public string ToJson()
  {
    JsonSerializerSettings settings = new JsonSerializerSettings();
    settings.DefaultValueHandling = DefaultValueHandling.Ignore;

    return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()), settings);
  }

  public virtual Object MsgData()
  {
    return this;
  }

  public abstract string MsgType();
}

public class Join : SendMsg
{
  public string name;
  public string key;

  public Join(string name, string key)
  {
    this.name = name;
    this.key = key;
  }

  public override string MsgType()
  {
    return "join";
  }
}

public class JoinRace : SendMsg
{
  public Join botId = null;
  public string trackName = "keimola";
  public string password = null;
  public int carCount = 1;

  public JoinRace(Join botId, string trackName, string password, int carCount)
  {
    this.botId = botId;
    this.trackName = trackName;
    this.password = password;
    this.carCount = carCount;
  }

  public override string MsgType()
  {
    return "joinRace";
  }
}

public class Ping : SendMsg
{
  public override string MsgType()
  {
    return "ping";
  }
}

public class Throttle : SendMsg
{
  public double value;

  public Throttle(double value)
  {
    this.value = value;
  }

  public override Object MsgData()
  {
    return this.value;
  }

  public override string MsgType()
  {
    return "throttle";
  }
}

public class SwitchLane : SendMsg
{
  public string lane;

  public SwitchLane(string lane)
  {
    this.lane = lane;
  }

  public override Object MsgData()
  {
    return this.lane;
  }

  public override string MsgType()
  {
    return "switchLane";
  }
}

public class Turbo : SendMsg
{
  public string message;

  public Turbo(string message)
  {
    this.message = message;
  }

  public override Object MsgData()
  {
    return this.message;
  }

  public override string MsgType()
  {
    return "turbo";
  }
}

public class PositionSpeed
{
  public int pieceIndex;
  public double pieceDistance;
  public double speed;

  public PositionSpeed(int pieceIndex, double pieceDistance, double speed)
  {
    this.pieceIndex = pieceIndex;
    this.pieceDistance = pieceDistance;
    this.speed = speed;
  }
}
