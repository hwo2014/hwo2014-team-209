﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

public class RaceStatus
{
  public List<Car> cars = null;
  public string gameId = null;
  public int gameTick = 0;

  public RaceStatus(List<Car> cars, string gameId, int gameTick)
  {
    this.cars = cars;
    this.gameId = gameId;
    this.gameTick = gameTick;
  }

  public Car GetCar(string color)
  {
    foreach (Car currentCar in this.cars)
    {
      if (currentCar.Color() == color) return currentCar;
    }

    return null;
  }
}

public class Car
{
  public CarId id = null;
  public double angle = 0.0;
  [JsonProperty("piecePosition")]
  public CarPosition position = null;

  public Car(CarId id, double angle, CarPosition position)
  {
    this.id = id;
    this.angle = angle;
    this.position = position;
  }

  public string Color()
  {
    return this.id.color;
  }

  public double GetSpeed(CarPosition previousPosition, Track track, int currentTick, int previousTick)
  {
    double distance = -1.0;

    if (this.position.pieceIndex == previousPosition.pieceIndex)
    {
      distance = this.position.inPieceDistance - previousPosition.inPieceDistance;
    }
    else
    {
      if (previousPosition.pieceIndex >= 0 && previousPosition.pieceIndex < track.pieces.Count &&
        previousPosition.lane.startLaneIndex == previousPosition.lane.endLaneIndex)
      {
        TrackPiece previousPiece = track.pieces[previousPosition.pieceIndex];
        TrackLane previousPieceLane = track.lanes[previousPosition.lane.endLaneIndex];

        double currentPieceDistance = this.position.inPieceDistance;
        double previousPieceLength = previousPiece.PieceLength(previousPieceLane.distanceFromCenter);
        double previousPieceStart = previousPosition.inPieceDistance;

        distance = currentPieceDistance + (previousPieceLength - previousPieceStart);
      }
    }

    double speed = -1.0;

    if (currentTick != previousTick)
    {
      speed = distance / (currentTick - previousTick);
    }

    return speed;
  }
}

public class CarId
{
  public string name = null;
  public string color = null;

  public CarId(string name, string color)
  {
    this.name = name;
    this.color = color;
  }
}

public class CarPosition
{
  public int pieceIndex = 0;
  public double inPieceDistance = 0.0;
  public Lane lane = null;
  public int lap = 0;

  public CarPosition(int pieceIndex, double inPieceDistance, Lane lane, int lap)
  {
    this.pieceIndex = pieceIndex;
    this.inPieceDistance = inPieceDistance;
    this.lane = lane;
    this.lap = lap;
  }
}

public class Lane
{
  public int startLaneIndex = 0;
  public int endLaneIndex = 0;

  public Lane(int startLaneIndex, int endLaneIndex)
  {
    this.startLaneIndex = startLaneIndex;
    this.endLaneIndex = endLaneIndex;
  }
}


