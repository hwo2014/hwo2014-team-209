﻿using System;
using System.Collections.Generic;

public class CarData
{
  public Dictionary<int, PieceData> pieceData = null;

  public CarData()
  {
    this.pieceData = new Dictionary<int, PieceData>();
  }
}

public class PieceData
{
  // First lap, then lane
  public Dictionary<int, Dictionary<int, LaneData>> laneData = null;

  public PieceData()
  {
    this.laneData = new Dictionary<int, Dictionary<int, LaneData>>();
  }
}

public class LaneData
{
  public List<SpeedData> speedData = null;

  public LaneData()
  {
    this.speedData = new List<SpeedData>();
  }
}

public class SpeedData
{
  public double pieceDistance = 0.0;
  public double speed = 0.0;
  public double slipAngle = 0.0;

  public SpeedData(double pieceDistance, double speed, double slipAngle)
  {
    this.pieceDistance = pieceDistance;
    this.speed = speed;
    this.slipAngle = slipAngle;
  }
}
